class CreateEventos < ActiveRecord::Migration[5.0]
  def change
    create_table :eventos do |t|
      t.string :name
      t.string :address
      t.string :hour
      t.string :website

      t.timestamps
    end
  end
end
