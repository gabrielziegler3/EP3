json.extract! evento, :id, :name, :address, :data, :hour, :website, :created_at, :updated_at
json.url evento_url(evento, format: :json)